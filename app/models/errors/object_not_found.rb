class ObjectNotFound < StandardError
  def initialize(model, id)
    super(model)
    @id = id
  end
end
